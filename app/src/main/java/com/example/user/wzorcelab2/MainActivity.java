package com.example.user.wzorcelab2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.List;

import domain.RecipeView;
import entity.Recipe;
import presentation.RecipePresenter;

public class MainActivity extends AppCompatActivity implements RecipeView {
    RecipePresenter recipePresenter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
         recipePresenter = new RecipePresenter();
        recipePresenter.onAttach(this);
        final Button button = findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                CheckBox meetCBox = findViewById(R.id.checkBox);
                getRecipes(meetCBox.isChecked());
            }
        });
    }

    @Override
    protected void onDestroy() {
        recipePresenter.onDetach();
        super.onDestroy();

    }


    @Override
    public void getRecipes(boolean isWithMeat) {
        final TextView view = findViewById(R.id.textView);
        view.setText("");
        List<Recipe> recipes = recipePresenter.showRecipes(isWithMeat);
        for (int i=0;i<recipes.size();i++)
            view.setText(view.getText()+ recipes.get(i).Name + " ["+recipes.get(i).Description+"]" + System.getProperty ("line.separator"));

    }
}
