package repository;

import java.util.ArrayList;
import java.util.List;

import entity.Recipe;

/**
 * Created by User on 2017-11-28.
 */

public class RecipeReposotory {
    public static List<Recipe> GetAll(){
        List<Recipe> data = new ArrayList<Recipe>();
        data.add(new Recipe("Żurek", "zupa z jajkiem i ziemniakami", true,false));
        data.add(new Recipe("Pierogi z mięsem", "Pierogi z mięsem", true,false));
        data.add(new Recipe("Pierogi russkie", "Pierogi", true,false));
        data.add(new Recipe("Zupa pomidorowa", "zupa", false,true));
        data.add(new Recipe("Salatka", "przystawki", false,true));
        data.add(new Recipe("Ryż", "przystawki", false,true));
    return data;
    }
}
